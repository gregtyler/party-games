/* eslint-env node */
const DISPATCH_DELAY = 5;

const locations = [
  {id: 'CORE-CHURCH', name: 'Church', roles: ['Pastor', 'Choir singer', 'Janitor', 'Grave digger', 'Sunday school teacher', 'Choir leader', 'Member of congregation', 'Bell ringer']},
  {id: 'CORE-WEDDING', name: 'Wedding', roles: ['Bride', 'Groom', 'Photographer', 'Father of the Bride', 'Groom\'s Uncle', 'Driver', 'Usher', 'Bridesmaid']},
  {id: 'CORE-CRUISE', name: 'Cruise', roles: ['Captain', 'Waiting staff', 'Entertainer', 'Cruise guest', 'Chef', 'Cabin steward', 'Chief Engineer', 'Child supervisor']},
  {id: 'CORE-OFFICE', name: 'Office', roles: ['Manager', 'Accountant', 'Lawyer', 'Payroll administrator', 'Lawyer', 'Sales assistant', 'Receptionist', 'Personal assistant']},
  {id: 'CORE-POLICE', name: 'Police station', roles: ['Police Community Support Officer', 'Sniffer dog', 'Confidential informant', 'Custody officer', 'Phone operator', 'Lawyer', 'Detective', 'Chief superintendent']},
  {id: 'CORE-SCHOOL', name: 'School', roles: ['English teacher', 'Head teacher', 'Teaching assistant', 'Librarian', 'Student', 'Caretaker', 'PE teacher', 'Nurse']},
  {id: 'CORE-CASINO', name: 'Casino', roles: ['Croupier', 'Pit boss', 'Waiting staff', 'High roller', 'Cashier', 'Security guard', 'Floor staff', 'Gambler']},
  {id: 'CORE-TV', name: 'TV set', roles: ['Director', 'Camera operator', 'Actor/actress', 'Script supervisor', 'Editor', 'Boom operator', 'Runner', 'TV executive']},
  {id: 'CORE-UNIVERSITY', name: 'University', roles: ['Lecturer', 'Cleaner', 'Vice Principal', 'Tenured professor', 'Research assistant', 'Medical student', 'Head of School of History', 'IT support officer']},
  {id: 'CORE-SPACESHIP', name: 'Spaceship', roles: ['Captain', 'Pilot', 'Engineer', 'Armourer', 'Gunner', 'Communications officer', 'Security officer', 'Chief scientist']},
  {id: 'CORE-CONFERENCE', name: 'Conference', roles: ['Motivational speaker', 'Delegate', 'AV technician', 'Caterer', 'Sales assistant', 'Keynote speaker', 'Venue manager', 'Welcoming staff']},
  {id: 'CORE-WILDWEST', name: 'Wild West', roles: ['Gunslinger', 'Bartender', 'Sheriff', 'Prisoner', 'Rancher', 'Train driver', 'Town drunk', 'Dancer']},
  {id: 'CORE-PIRATES', name: 'Pirate ship', roles: ['Captain', 'Lookout', 'First mate', 'Floor swabber', 'Prisoner', 'Parrot', 'Armourer', 'Chef']},
  {id: 'CORE-VICTORIAN', name: 'Victorian stately home', roles: ['Chimney sweep', 'Maid', 'Butler', 'Lord of the Manor', 'Cook', 'Groundskeeper', 'Chauffeur', 'Nanny']}
];

/**
 * Return a shuffled version of the given array;
 * @param {array} arr The array to shuffle
 * @returns {array} The shuffled array
 */
function shuffle(arr) {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }

  return arr;
}

/**
 * Extract publicly viewable location information (e.g. don't share role details
 * with clients)
 * @param {object} location The location to strip back
 * @returns {object} The location with any private information removed
 */
function publicLocationInfo(location) {
  return {id: location.id, name: location.name};
}

module.exports = function server(roomEmit, users) {
  const spy = users[Math.floor(Math.random() * users.length)];
  const potentialLocations = shuffle(locations).slice(0, 10);
  const location = potentialLocations[Math.floor(Math.random() * potentialLocations.length)];
  const roles = shuffle(location.roles);

  const votes = [];

  // Wait for people to read the instructions, then send roles
  setTimeout(() => {
    users.forEach(user => {
      if (user === spy) {
        user.socket.emit('SPYFALL:DISPATCH_SPY', {choices: potentialLocations.map(publicLocationInfo)});

        user.socket.on('SPYFALL:GUESS_LOCATION', function(locationId) {
          if (locationId === location.id) {
            roomEmit('SPYFALL:RESOLVE', {isSpyWin: true, isVoteResult: false, spyId: spy.id});
          } else {
            roomEmit('SPYFALL:RESOLVE', {isSpyWin: false, isVoteResult: false, spyId: spy.id});
          }
        });
      } else {
        user.socket.emit('SPYFALL:DISPATCH_INNOCENT', {location: publicLocationInfo(location), choices: potentialLocations.map(publicLocationInfo), role: roles.pop()});
      }

      user.socket.on('SPYFALL:VOTE_START', function(accusedId) {
        if (accusedId !== user.id && !votes.find(v => v.leaderId === user.id) && !votes.find(v => v.accusedId === accusedId)) {
          const vote = {
            id: votes.length + 1,
            leaderId: user.id,
            accusedId: accusedId,
            votes: [{userId: user.id, verdict: true}]
          };

          votes.push(vote);
          roomEmit('SPYFALL:VOTE_STARTED', vote);
        }
      });

      user.socket.on('SPYFALL:VOTE_PARTICIPATE', function({voteId, verdict}) {
        const vote = votes.find(v => v.id === voteId);
        if (!vote.votes.find(v => v.userId === user.id) && vote.accusedId !== user.id) {
          vote.votes.push({
            userId: user.id,
            verdict
          });
        }

        roomEmit('SPYFALL:VOTE_BROADCAST', {voteId, userId: user.id, verdict});

        if (vote.votes.length === users.length - 1) {
          const isUnanimous = vote.votes.every(v => v.verdict === true);
          roomEmit('SPYFALL:VOTE_CONCLUDED', {voteId: vote.id, votes: vote.votes, isUnanimous});
          if (isUnanimous) {
            if (vote.accusedId === spy.id) {
              roomEmit('SPYFALL:RESOLVE', {isSpyWin: false, isVoteResult: true, spyId: spy.id});
            } else {
              roomEmit('SPYFALL:RESOLVE', {isSpyWin: true, isVoteResult: true, spyId: spy.id});
            }
          }
        }
      });
    });
  }, DISPATCH_DELAY * 1000);
};
