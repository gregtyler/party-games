export const DISPATCH_SPY = 'DISPATCH_SPY';
export const DISPATCH_INNOCENT = 'DISPATCH_INNOCENT';
export const GUESS_LOCATION = 'GUESS_LOCATION';
export const RESOLVE = 'RESOLVE';
export const VOTE_START = 'VOTE_START';
export const VOTE_STARTED = 'VOTE_STARTED';
export const VOTE_PARTICIPATE = 'VOTE_PARTICIPATE';
export const VOTE_BROADCAST = 'VOTE_BROADCAST';
export const VOTE_CONCLUDED = 'VOTE_CONCLUDED';
