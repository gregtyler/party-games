import {DISPATCH_SPY, DISPATCH_INNOCENT, GUESS_LOCATION, RESOLVE, VOTE_START, VOTE_STARTED, VOTE_PARTICIPATE, VOTE_BROADCAST, VOTE_CONCLUDED} from './mutation-types';
import {TERMINATE_GAME} from '../../store/mutation-types.js';

export default {
  state: {
    id: 'SPYFALL',
    name: 'Spyfall',
    players: [3, 8],
    hasRole: false,
    role: null,
    choices: [],
    isSpy: false,
    location: null,
    isRoundComplete: false,
    spyId: null,
    isSpyWin: null,
    isVoteResult: null,
    votes: []
  },
  getters: {
    spyfallSpy(state, getters, rootState) {
      return rootState.core.players.find(player => player.id === state.spyId);
    }
  },
  mutations: {
    [DISPATCH_SPY]: function(state, {choices}) {
      state.choices = choices;
      state.isSpy = true;
      state.hasRole = true;
    },
    [DISPATCH_INNOCENT]: function(state, {location, choices, role}) {
      state.location = location;
      state.choices = choices;
      state.role = role;
      state.hasRole = true;
    },
    [RESOLVE]: function(state, {isSpyWin, isVoteResult, spyId}) {
      state.isRoundComplete = true;
      state.spyId = spyId;
      state.isSpyWin = isSpyWin;
      state.isVoteResult = isVoteResult;
      this.getters.socket.emit(TERMINATE_GAME);
    },
    [GUESS_LOCATION](state, {location}) {
      this.getters.socket.emit(`${state.id}:${GUESS_LOCATION}`, location);
    },
    [VOTE_START](state, {accusedId}) {
      this.getters.socket.emit(`${state.id}:${VOTE_START}`, accusedId);
    },
    [VOTE_STARTED](state, voteObj) {
      voteObj.isActive = true;
      state.votes.push(voteObj);
    },
    [VOTE_PARTICIPATE](state, {voteId, verdict}) {
      this.getters.socket.emit(`${state.id}:${VOTE_PARTICIPATE}`, {voteId, verdict});
    },
    [VOTE_BROADCAST](state, {voteId, userId, verdict}) {
      const vote = state.votes.find(v => v.id === voteId);
      vote.votes.push({userId, verdict});
    },
    [VOTE_CONCLUDED](state, {voteId, votes, isUnanimous}) {
      const vote = state.votes.find(v => v.id === voteId);
      vote.isActive = false;
      vote.votes = votes;
      vote.isUnanimous = isUnanimous;
    }
  }
};
