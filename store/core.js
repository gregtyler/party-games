import {GET_GAME_DETAILS, CONNECT_ERROR, IDENTIFY, PLAYER_JOIN, PLAYER_LEAVE, PLAYER_NAME, PLAYER_READY, SET_READY, SET_NAME, GAME_START, GAME_END} from './mutation-types.js';
import addMutations from '../src/addMutations';

let socket;

/**
 * Identify the game being joined, if set
 */
function getGameId() {
  const parts = window.location.search.substr(1).split('&');
  let gameId = null;
  parts.forEach((part) => {
    const [id, ...values] = part.split('=');
    const value = values.join('=');

    if (id === 'game') {
      gameId = value.toUpperCase();
    }
  });

  return gameId;
}

export default {
  state: {
    gameId: getGameId(),
    gameMode: null,
    isStarted: false,
    isErrored: false,
    currentId: null,
    players: []
  },
  getters: {
    self(state) {
      if (state.currentId === null) return null;
      return state.players.find(p => p.id === state.currentId);
    },
    game(state, getters, rootState) {
      return rootState[state.gameMode];
    },
    players(state) {
      return state.players.filter(player => player.isConnected);
    },
    socket() {
      return socket;
    }
  },
  actions: {
    [GET_GAME_DETAILS](context) {
      socket = io({
        query: {
          gid: getGameId(),
          uid: localStorage.getItem('player-uid')
        }
      });

      addMutations(socket, function(mutation) {
        return function(player) {
          context.commit(mutation, player);
        };
      });

      return Promise.resolve();
    }
  },
  mutations: {
    [GET_GAME_DETAILS](state, gameMode) {
      state.gameMode = gameMode;
      state.isErrored = false;
    },
    [CONNECT_ERROR](state) {
      state.isErrored = true;
    },
    [IDENTIFY](state, {id}) {
      state.currentId = id;
      localStorage.setItem('player-uid', id);
    },
    [PLAYER_JOIN](state, player) {
      state.players.push(player);
    },
    [PLAYER_READY](state, {id, isReady}) {
      const player = state.players.find(p => p.id === id);
      player.isReady = isReady;
    },
    [PLAYER_LEAVE](state, {id}) {
      const player = state.players.find(p => p.id === id);
      player.isConnected = false;
    },
    [PLAYER_NAME](state, {id, name}) {
      const player = state.players.find(p => p.id === id);
      player.name = name;
    },
    [SET_READY](state, isReady) {
      this.getters.socket.emit(SET_READY, isReady);
    },
    [SET_NAME](state, name) {
      this.getters.socket.emit(SET_NAME, name);
    },
    [GAME_START](state) {
      state.players.reduceRight((acc, u, index, arr) => {
        if (!u.isConnected) {
          arr.splice(index, 1);
        }
      }, []);

      state.isStarted = true;
    },
    [GAME_END](state) {
      state.isStarted = false;
    }
  }
};
