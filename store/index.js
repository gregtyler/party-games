import Vuex from 'vuex';
import Vue from 'vue';
import core from './core.js';
import spyfall from '../games/spyfall/store.js';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  modules: {
    core,
    spyfall
  }
});
