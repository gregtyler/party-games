export const GET_GAME_DETAILS = 'GET_GAME_DETAILS';
export const CONNECT_ERROR = 'CONNECT_ERROR';
export const IDENTIFY = 'IDENTIFY';
export const PLAYER_JOIN = 'PLAYER_JOIN';
export const PLAYER_NAME = 'PLAYER_NAME';
export const PLAYER_LEAVE = 'PLAYER_LEAVE';
export const PLAYER_READY = 'PLAYER_READY';
export const SET_NAME = 'SET_NAME';
export const SET_READY = 'SET_READY';
export const TERMINATE_GAME = 'TERMINATE_GAME';
export const GAME_START = 'GAME_START';
export const GAME_END = 'GAME_END';

import * as spyfallMutations from '../games/spyfall/mutation-types.js';
export const SPYFALL =  spyfallMutations;
