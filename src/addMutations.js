import * as mutationTypes from '../store/mutation-types.js';

/**
 * Add mutations
 */
export default function addMutations(socket, callback, mutations, prefix) {
  if (typeof mutations === 'undefined') mutations = mutationTypes;

  for (let key in mutations) {
    const mutation = mutations[key];

    if (typeof mutation === 'object') {
      addMutations(socket, callback, mutation, key);
    } else {
      let event;
      if (typeof prefix === 'string') {
        event = prefix + ':' + mutation;
      } else {
        event = mutation;
      }

      socket.on(event, callback.call(window, mutation));
    }
  }
}
