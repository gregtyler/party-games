/* eslint-env node */
const fs = require('fs');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const storage = require('node-persist');
const app = express();
const http = require('http').Server(app); // eslint-disable-line new-cap
const io = require('socket.io')(http);
const PORT = 9447;

const serverSpyfall = require('./games/spyfall/server.js');

// Decode the mutation types file, which is written in ES6
const actions = {};
const mutationFile = fs.readFileSync(path.join(__dirname, 'store/mutation-types.js'));
const mutationPattern = /export const (.*) = '(.*)';/g;
let match;
while (match = mutationPattern.exec(mutationFile.toString())) {
  actions[match[1]] = match[2];
}

// Set up a static directory
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: false}));

/**
 * Generate a random ID for the game
 */
function genGameId() {
  return [0, 1, 2, 3].map(() => String.fromCharCode((Math.random() * 26 >> 1) + 65)).join('');
}

storage.initSync();
let games = storage.getItemSync('games');

/**
 * Save game data
 */
function saveGames() {
  storage.setItemSync('games', games);
}

if (!games) {
  games = [];
  saveGames();
}

io.on('connection', function(socket) {
  const game = games.find(g => g.id === socket.handshake.query.gid);

  if (!game) {
    socket.emit(actions.CONNECT_ERROR);
    return;
  }

  const users = game.users;
  const sockets = game.sockets;

  // Join the game's room
  const roomId = `game:${game.id}`;
  socket.join(roomId);

  // A utility function to broadcast to the current room
  const roomEmit = function roomEmit(message, data) {
    return io.to(roomId).emit(message, data);
  };

  let user = null;
  if (socket.handshake.query.uid) {
    const tryUser = game.users.find(u => u.id === socket.handshake.query.uid);
    if (tryUser && !tryUser.isConnected) {
      user = tryUser;
      user.isConnected = true;
    }
  }

  if (user === null) {
    user = {id: socket.id, isHost: false, isReady: false, isConnected: true};

    user.order = Math.max.apply(process, users.map(user => user.order).concat([0])) + 1;
    user.name = `Player ${user.order}`;
    users.push(user);

    if (!users.find(u => u.isHost)) {
      user.isHost = true;
    }
  }

  sockets[user.id] = socket;

  socket.emit(actions.IDENTIFY, {id: user.id});
  socket.emit(actions.GET_GAME_DETAILS, game.mode);

  roomEmit(actions.PLAYER_JOIN, user);

  if (users.every(u => u.isReady)) {
    socket.emit(actions.GAME_START);
  }

  users.forEach(u => {
    if (user.id !== u.id) {
      socket.emit(actions.PLAYER_JOIN, u);
    }
  });

  socket.on(actions.SET_NAME, function(name) {
    user.name = name;
    roomEmit(actions.PLAYER_NAME, {id: user.id, name});
  });

  socket.on(actions.SET_READY, function(isReady) {
    user.isReady = isReady;
    roomEmit(actions.PLAYER_READY, {id: user.id, isReady});

    if (users.filter(u => u.isConnected).every(u => u.isReady)) {
      users.reduceRight((acc, u, index, arr) => {
        if (!u.isConnected) {
          arr.splice(index, 1);
        }
      }, []);

      roomEmit(actions.GAME_START);

      if (user.isHost) {
        const usersWithSockets = users.map(u => Object.assign({}, u, {socket: sockets[u.id]}));
        serverSpyfall(roomEmit, usersWithSockets);
      }
    }
  });

  socket.on(actions.TERMINATE_GAME, function() {
    setTimeout(() => {
      roomEmit(actions.GAME_END);

      users.forEach(user => {
        user.isReady = false;
        roomEmit(actions.PLAYER_READY, {id: user.id, isReady: false});
      });
    }, 3000);
  });

  socket.on('disconnect', function() {
    roomEmit(actions.PLAYER_LEAVE, {id: user.id});
    user.isConnected = false;
  });

  saveGames();
});

app.post('/new', (req, res) => {
  // Start a new game
  const id = genGameId();
  const mode = req.body['game-mode'];
  games.push({id, users: [], sockets: [], mode});
  saveGames();
  res.redirect('/?game=' + id);
});

http.listen(PORT, function() {
  console.log(`listening on *:${PORT}`);
});
